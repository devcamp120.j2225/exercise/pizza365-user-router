//khai báo thư viện express
const express = require('express');
const { userMiddleware } = require('../middlewares/userMiddleware');

//tạo router
const userRouter = express.Router();

//sủ dụng middle ware
userRouter.use(userMiddleware);

//get all courses
userRouter.get('/users', (request, response) => {
    console.log("Get all users");
    response.json({
        message:'Get All users'
    })
});

//get a course
userRouter.get('/users/:userid', (request, response) => {
    let id = request.params.userid;

    console.log(`Get All userid = ${id}`);
    response.json({
        message:`Get All userid = ${id}`
    })
})

//create a course
userRouter.post('/users', (request, response) => {
    let body = request.body;

    console.log('create a user');
    console.log(body);
    response.json({
        ...body
    })
});


//update a course
userRouter.put('/users/:userid', (request, response) => {
    let id = request.params.userid;
    let body = request.body;

    console.log('update a user');
    console.log({id, ...body});
    response.json({
        message: {id, ...body}
    })
})

//delete a course
userRouter.delete('/users/:userid', (request, response) => {
    let id = request.params.userid;

    console.log('delete a user ' + id);
    response.json({
        message: 'delete a user ' + id
    })
})

module.exports = { userRouter };