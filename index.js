//khai báo thư viện express
const express = require('express');
const { userRouter } = require('./app/routes/userRouter');

//khởi tạo ứng dụng nodejs
const app = new express();

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//khai báo port chạy nodejs
const port = 8000;

app.get('/', (request, response) => {
    let today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`);
    
    response.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
    })
})

//sử dụng router
app.use('/', userRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})